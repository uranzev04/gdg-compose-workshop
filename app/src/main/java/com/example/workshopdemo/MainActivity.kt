package com.example.workshopdemo

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.workshopdemo.ui.theme.WorkshopDemoTheme
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getMovies()
        val countState = mutableIntStateOf(0)

        setContent {
            val viewModel = viewModel<CountViewModel>()

            WorkshopDemoTheme {
//              MyCountWithDependency(count = countState.intValue , buttonClick = {
//                    countState.value = it
//                  Toast.makeText(this, "Count: ${it}", Toast.LENGTH_SHORT).show()
//
//              } )
//                CountWithViewModel(count = viewModel.count, buttonClick = {
//                    viewModel.updateCount(it)
//                } )
                TextList()
            }
        }
    }
}



@Preview(showBackground = true)
@Composable
fun MyPreview() {
    WorkshopDemoTheme {
        TextList()
    }
}



private fun getMovies(){
    try {
        val response = RetrofitInstance.api.getMovieListByTitle("war")
        Log.e("Response", response.request().toString())
        response.enqueue(object : Callback<MovieList> {
            override fun onResponse(call: Call<MovieList>, response: Response<MovieList>) {
                Log.e("Response", response.body().toString())
                if (response.isSuccessful && response.body()?.Response == "True") {
                    val movieList = response.body()
                    if (movieList != null) {
                        println(movieList)
                    }
                } else {
                    println(response.errorBody())
                }
            }
            override fun onFailure(call: Call<MovieList>, t: Throwable) {
                println(t.message)
            }
        })

    } catch (e: Exception) {
        println(e.message)
    }
}