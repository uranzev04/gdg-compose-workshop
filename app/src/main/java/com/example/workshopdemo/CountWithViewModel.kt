package com.example.workshopdemo

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

@Composable
fun CountWithViewModel(count:Int, buttonClick:(Int) ->Unit){
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.White),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text("UniDiriection Count: ${count}")
        Button(onClick = {
//            Toast.makeText(context, "Count: $count", Toast.LENGTH_SHORT).show()
            val mycount = count + 1
            buttonClick(mycount)
        }) {
            Text("Click me")

        }
    }
}