package com.example.workshopdemo

import androidx.compose.runtime.mutableIntStateOf
import androidx.lifecycle.ViewModel

class CountViewModel:ViewModel() {
    private var _count = mutableIntStateOf(0)

    val count get() = _count.intValue


    fun updateCount(newCount:Int){
        _count.intValue = newCount
    }
}