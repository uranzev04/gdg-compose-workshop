package com.example.workshopdemo

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
val countState = mutableIntStateOf(0)

@Composable
fun MyCount(){
    var count = 0
    val context = LocalContext.current
    val myCountState = remember { mutableIntStateOf(0) }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.White),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text("Count: ${myCountState.intValue}")
        Button(onClick = {
            count++
            myCountState.intValue = myCountState.intValue + 1
//            Toast.makeText(context, "Count: $count", Toast.LENGTH_SHORT).show()
            Toast.makeText(context, "Count: ${myCountState.intValue}", Toast.LENGTH_SHORT).show()
        }) {
            Text("Click me")

        }
    }
}