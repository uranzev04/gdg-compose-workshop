package com.example.workshopdemo

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun TextList(){
    Column(
        modifier = Modifier
            .background(color = Color.White)
            .fillMaxSize(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text("Simple Text")
        Text(stringResource(id = R.string.app_name))
        Text("Styled Text",
            color = Color.Cyan,
            fontSize = 30.sp,
            fontStyle = FontStyle.Italic,
        )

        Text("Shadow text",
            style = TextStyle(
                color = Color.Red,
                fontSize = 30.sp,
                fontStyle = FontStyle.Italic,
                shadow = Shadow(
                    color = Color.Black,
                    blurRadius = 10f,
                    offset = Offset(5f, 5f)
                )
            )
        )

        Text("Brush Text, Caution: The current usage of " +
                "the Brush API in TextStyle is experimental. " +
                "Experimental APIs can change in the future.",
            textAlign = TextAlign.Justify,
            modifier = Modifier.padding(100.dp),
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                brush = Brush.linearGradient(
                    colors = listOf(Color.Cyan, Color.Blue, Color.Yellow)
                ),
            )
        )




    }
}
