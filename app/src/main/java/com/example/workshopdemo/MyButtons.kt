package com.example.workshopdemo

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp

@Composable
fun ButtonList(){
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .background(color = Color.White)
            .fillMaxSize(),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Button(onClick = {
            Log.e("button", "button 1 clicked")
            Toast.makeText(context, "button 1 clicked", Toast.LENGTH_SHORT).show()
        }) {
            Text("normal button")
        }
        FilledTonalButton(onClick = {
            Log.e("button", "button 2 clicked")
        }) {
            Text("tonal button")
        }

        OutlinedButton(onClick = {
            Log.e("button", "button 3 clicked")
        }) {
            Text("outlined button")
        }

        ElevatedButton(onClick = {
            Log.e("button", "button 4 clicked")
        }) {
            Text("elevated button")
        }

        TextButton(onClick = {
            Log.e("button", "button 5 clicked")
        }, colors = ButtonDefaults.buttonColors(Color.Blue),
            shape = RoundedCornerShape(4.dp)
        ) {
            Text("text button")
        }




    }
}
