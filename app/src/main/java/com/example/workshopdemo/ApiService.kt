package com.example.workshopdemo

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("i={i}")
    fun getMovieDetail(@Path("i") id: String, @Query("apikey") key:String = "b2f11168" ): Call<MovieDetail>

    @GET("/")
    fun getMovieListByTitle(@Query("s") search: String, @Query("apikey") key:String = "b2f11168"): Call<MovieList>
}