package com.example.workshopdemo

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun LazyColumnTexts(){
    LazyColumn{
        items(100){
            Text("Text $it",
                modifier = Modifier.padding(10.dp),
                style = MaterialTheme.typography.headlineMedium)
            Divider()
        }
    }
}